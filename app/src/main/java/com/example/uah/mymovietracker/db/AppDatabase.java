package com.example.uah.mymovietracker.db;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.uah.mymovietracker.db.converter.DateConverter;
import com.example.uah.mymovietracker.db.dao.FilmDao;
import com.example.uah.mymovietracker.db.entity.FilmEntity;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * Clase para definir la conexión a la BBDD de Room. Room ofrece una capa de abstracción por encima
 * de la BBDD de SQLite, a fin de simplificar las operaciones que tendrían que realizarse con la
 * clase SQLiteOpenHelper.
 * <p>
 * Fuente: https://developer.android.com/codelabs/android-room-with-a-view#7
 */
// Se especifica que la base de datos va a estar formada por la tabla que se define en la entidad de la clase Film
@Database(entities = {FilmEntity.class}, version = 1, exportSchema = false)
@TypeConverters({DateConverter.class})
public abstract class AppDatabase extends RoomDatabase {
    // Instancia Singleton para evitar tener múltiples instancias de la BBDD abiertas simultáneamente
    private static AppDatabase sInstance;

    public static final String DATABASE_NAME = "film_database";

    // Room utiliza la clase expuesta con @Dao para hacer las consultas a la BBDD
    public abstract FilmDao filmDao();

    // ExecutorServive va a permitir ejecutar las operaciones de BBDD en varios hilos de forma
    // asíncrona en segundo plano
    private static final int NUMBER_OF_THREADS = 4;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    private final MutableLiveData<Boolean> mIsDatabaseCreated = new MutableLiveData<>();

    /**
     * Método para obtener el objeto Singleton. La primera vez que se acceda a él, creará la base de
     * datos
     *
     * @param context
     * @return
     */
    public static AppDatabase getInstance(final Context context) {
        if (sInstance == null) {
            synchronized (AppDatabase.class) {
                if (sInstance == null) {
                    sInstance = buildDatabase(context.getApplicationContext());
                    sInstance.updateDatabaseCreated(context.getApplicationContext());
                }
            }
        }
        return sInstance;
    }

    /**
     * Envía un evento avisando de que la conexión a la base de datos ha sido inicializada
     */
    private void setDatabaseCreated() {
        mIsDatabaseCreated.postValue(true);
    }

    /**
     * Cambia el estado de la conexión a la base de datos
     *
     * @param context
     */
    private void updateDatabaseCreated(final Context context) {
        if (context.getDatabasePath(DATABASE_NAME).exists()) {
            setDatabaseCreated();
        }
    }

    /**
     * Método que se encarga de crear la conexión a la base de datos y comunicar el estado de dicha
     * conexión tras la aplicación.
     *
     * @param appContext
     * @return
     */
    private static AppDatabase buildDatabase(final Context appContext) {
        return Room.databaseBuilder(appContext, AppDatabase.class, DATABASE_NAME)
                .addCallback(new Callback() {
                    @Override
                    public void onCreate(@NonNull SupportSQLiteDatabase db) {
                        super.onCreate(db);
                        AppDatabase database = AppDatabase.getInstance(appContext);
                        database.setDatabaseCreated();
                    }
                })
                .allowMainThreadQueries()
                .build();
    }


}
