package com.example.uah.mymovietracker.io.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Clase para convertir la respuesta recibida de una petición mediante Retrofit a la API de búsquedas
 * por nombre de OMDB
 */
public class OmdbSearch {

    /** Atributos */
    @SerializedName("Search")
    @Expose
    private ArrayList<OmdbFilm> results;

    /** Constructor */
    public OmdbSearch(ArrayList<OmdbFilm> results) {

        this.results = results;
    }

    /** Metodos get y set */
    public ArrayList<OmdbFilm> getResults() {
        return results;
    }

    public void setResults(ArrayList<OmdbFilm> results) {
        this.results = results;
    }
}
