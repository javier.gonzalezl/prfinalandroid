package com.example.uah.mymovietracker.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.uah.mymovietracker.MyMovieTrackerApp;
import com.example.uah.mymovietracker.R;
import com.example.uah.mymovietracker.io.response.OmdbFilm;
import com.example.uah.mymovietracker.ui.activity.OmdbFilmActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class OmdbFilmListAdapter extends RecyclerView.Adapter<OmdbFilmListAdapter.OmdbFilmViewHolder>{
    private ArrayList<OmdbFilm> mFilmList;
    private LayoutInflater mInflater;

    class OmdbFilmViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public final TextView filmTitle;
        public final ImageView filmImage;
        public final TextView filmYear;

        final OmdbFilmListAdapter mAdapter;

        public OmdbFilmViewHolder(View itemView, OmdbFilmListAdapter adapter) {
            super(itemView);
            filmTitle = itemView.findViewById(R.id.title_sl);
            filmImage = itemView.findViewById(R.id.image_sl);
            filmYear = itemView.findViewById(R.id.year_sl);
            this.mAdapter = adapter;
            itemView.setOnClickListener(this);
        }

        @Override
        // Al hacer click en un elemento del RecyclerView se va a querer iniciar la actividad
        // de detalles de uno de los resultados de la búsqueda.
        public void onClick(View v) {
            // Se coge la posición del elemento pulsado para, a continuación, acceder al objeto vinculado
            int mPosition = getLayoutPosition();
            OmdbFilm element = mFilmList.get(mPosition);

            // Se crea el intent para iniciar la acitividad de detalles de la película buscada
            Intent intent = new Intent(v.getContext(), OmdbFilmActivity.class);
            intent.putExtra(MyMovieTrackerApp.EXTRA_OMDB_ID, element.getImdbID());
            v.getContext().startActivity(intent);
        }
    }

    public OmdbFilmListAdapter(Context context, ArrayList<OmdbFilm> filmList) {
        mInflater = LayoutInflater.from(context);
        this.mFilmList = filmList;
    }

    @Override
    public OmdbFilmListAdapter.OmdbFilmViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mItemView = mInflater.inflate(R.layout.searchlist_item, parent, false);
        return new OmdbFilmListAdapter.OmdbFilmViewHolder(mItemView, this);
    }

    @Override
    public void onBindViewHolder(OmdbFilmListAdapter.OmdbFilmViewHolder holder, int position) {
        OmdbFilm mCurrent = mFilmList.get(position);
        holder.filmTitle.setText(mCurrent.getTitle());
        holder.filmYear.setText(mCurrent.getYear());
        Picasso.get().load(mCurrent.getImage()).error(R.drawable.ic_baseline_broken_image_24).into(holder.filmImage);
    }

    @Override
    public int getItemCount() {
        return mFilmList.size();
    }

    public ArrayList<OmdbFilm> getFilmList() {
        return mFilmList;
    }

    public void setFilmList(ArrayList<OmdbFilm> mFilmList) {
        this.mFilmList = mFilmList;
    }
}
