package com.example.uah.mymovietracker.repository;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.example.uah.mymovietracker.db.AppDatabase;
import com.example.uah.mymovietracker.db.entity.FilmEntity;
import com.example.uah.mymovietracker.io.OmdbFilmsApiAdapter;
import com.example.uah.mymovietracker.io.OmdbFilmsApiService;
import com.example.uah.mymovietracker.io.response.OmdbFilm;
import com.example.uah.mymovietracker.io.response.OmdbSearch;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

/**
 * Implementación del patrón Repository. Esta clase abstrae el acceso a las diferentes fuentes
 * de datos de la apliación. En nuestro caso, la base de datos y la API de OMDB.
 * <p>
 * Fuente: https://developer.android.com/codelabs/android-room-with-a-view#8
 */
public class FilmRepository implements FilmRepositoryInterface {

    private static FilmRepository sInstance;
    private static OmdbFilmsApiService mApi;
    private final AppDatabase mDatabase;

    private static String API_KEY = "e708a288";
    private static String SEARCH_TYPE = "movie";

    private FilmRepository(final AppDatabase database) {
        mDatabase = database;
        mApi = OmdbFilmsApiAdapter.getApiService();
    }

    public static FilmRepository getInstance(final AppDatabase database) {
        if (sInstance == null) {
            synchronized (FilmRepository.class) {
                if (sInstance == null) {
                    sInstance = new FilmRepository(database);
                }
            }
        }
        return sInstance;
    }

    @Override
    /**
     * Método para obtener todas las películas de la BBDD
     */
    public ArrayList<FilmEntity> getAllFilms() {
        return (ArrayList<FilmEntity>) mDatabase.filmDao().getAllFilms();
    }

    @Override
    /**
     * Método para obtener una película de la BBDD a partir de su id
     */
    public FilmEntity getFilmById(int id) {
        return mDatabase.filmDao().getFilmById(id);
    }

    @Override
    /**
     * Método para almacenar una película en la BBDD
     */
    public void saveFilm(FilmEntity film) {
        mDatabase.databaseWriteExecutor.execute(() -> {
            mDatabase.filmDao().insertFilm(film);
        });
    }

    @Override
    /**
     * Método para actualizar una película en la BBDD
     */
    public void updateFilm(FilmEntity film) {
        mDatabase.databaseWriteExecutor.execute(() -> {
            mDatabase.filmDao().updateFilm(film);
        });
    }

    @Override
    /**
     * Método para eliminar una película de la BBDD
     */
    public void deleteFilm(FilmEntity film) {
        mDatabase.databaseWriteExecutor.execute(() -> {
            mDatabase.filmDao().deleteFilm(film);
        });
    }

    @Override
    /**
     * Método para obtener una película concreta a través de su omdbId en la API de OMDB
     */
    public Call<OmdbFilm> searchFilmByOmdbId(String omdbId) {
        Call<OmdbFilm> call = mApi.getFilmByOmdbId(API_KEY, omdbId);
        return call;
    }

    @Override
    /**
     * Método para obtener un listado de películas que coincidan con el nombre enviado como parámetro
     * en la API de OMDB
     */
    public Call<OmdbSearch> searchFilmsByName(String title) {
        Call<OmdbSearch> call = mApi.getFilmsByName(API_KEY, title, SEARCH_TYPE);
        return call;
    }

}