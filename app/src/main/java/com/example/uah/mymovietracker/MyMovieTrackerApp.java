package com.example.uah.mymovietracker;

import android.app.Application;

import com.example.uah.mymovietracker.db.AppDatabase;
import com.example.uah.mymovietracker.repository.FilmRepository;

/**
 * Clase para inicializar los Singleton de la aplicación
 */
public class MyMovieTrackerApp extends Application {

    public static final String EXTRA_OMDB_ID = "com.example.uah.misnotas.extra.OMDB_ID";
    public static final String EXTRA_ID = "com.example.uah.misnotas.extra.ID";

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public AppDatabase getDatabase() {
        return AppDatabase.getInstance(this);
    }

    public FilmRepository getRepository() {
        return FilmRepository.getInstance(getDatabase());
    }

}
