package com.example.uah.mymovietracker.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.uah.mymovietracker.MyMovieTrackerApp;
import com.example.uah.mymovietracker.R;
import com.example.uah.mymovietracker.db.entity.FilmEntity;
import com.example.uah.mymovietracker.ui.activity.FilmActivity;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class FilmListAdapter extends RecyclerView.Adapter<FilmListAdapter.FilmViewHolder> {

    private ArrayList<FilmEntity> mFilmList;
    private LayoutInflater mInflater;

    class FilmViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public final TextView filmTitle;
        public final ImageView filmImage;
        public final TextView filmActors;
        public final TextView filmYear;
        public final TextView filmWatchedInfo;

        private final Context mContext;

        final FilmListAdapter mAdapter;

        public FilmViewHolder(View itemView, FilmListAdapter adapter) {
            super(itemView);
            filmTitle = itemView.findViewById(R.id.title_fl);
            filmImage = itemView.findViewById(R.id.image_fl);
            filmActors = itemView.findViewById(R.id.actors_fl);
            filmYear = itemView.findViewById(R.id.year_fl);
            filmWatchedInfo = itemView.findViewById(R.id.watched_info_fl);
            mContext = itemView.getContext();
            this.mAdapter = adapter;
            itemView.setOnClickListener(this);
        }

        @Override
        // Al hacer click en un elemento del RecyclerView se va a querer iniciar la actividad
        // de detalles de una película.
        public void onClick(View v) {
            // Se coge la posición del elemento pulsado para, a continuación, acceder al objeto vinculado
            int mPosition = getLayoutPosition();
            FilmEntity element = mFilmList.get(mPosition);

            // Se crea el intent para iniciar la acitividad de detalles de la película
            Intent intent = new Intent(v.getContext(), FilmActivity.class);
            intent.putExtra(MyMovieTrackerApp.EXTRA_ID, element.getId());
            v.getContext().startActivity(intent);
        }
    }

    public FilmListAdapter(Context context, ArrayList<FilmEntity> filmList) {
        mInflater = LayoutInflater.from(context);
        this.mFilmList = filmList;
    }

    @Override
    public FilmViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mItemView = mInflater.inflate(R.layout.filmlist_item, parent, false);
        return new FilmViewHolder(mItemView, this);
    }

    @Override
    public void onBindViewHolder(FilmViewHolder holder, int position) {
        FilmEntity mCurrent = mFilmList.get(position);
        holder.filmTitle.setText(mCurrent.getTitle());
        holder.filmYear.setText(mCurrent.getYear());
        holder.filmActors.setText(mCurrent.getActors());

        DateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy");
        String dateFormatted = dateFormat.format(mCurrent.getDateWatched());
        holder.filmWatchedInfo.setText(holder.mContext.getString(R.string.filmlist_watched_info, dateFormatted, mCurrent.getPlaceWatched() ) );

        Picasso.get().load(mCurrent.getPoster()).error(R.drawable.ic_baseline_broken_image_24).into(holder.filmImage);
    }

    @Override
    public int getItemCount() {
        return mFilmList.size();
    }

    public ArrayList<FilmEntity> getFilmList() {
        return mFilmList;
    }

    public void setFilmList(ArrayList<FilmEntity> mFilmList) {
        this.mFilmList = mFilmList;
    }
}
