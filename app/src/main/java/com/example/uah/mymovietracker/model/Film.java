package com.example.uah.mymovietracker.model;

import java.util.Date;

/**
 * Interfaz para definir la entidad Film en la aplicación
 */
public interface Film {
    int getId();
    String getOmdbId();
    String getTitle();
    String getActors();
    String getYear();
    String getPoster();
    Date getDateWatched();
    String getPlaceWatched();
}
