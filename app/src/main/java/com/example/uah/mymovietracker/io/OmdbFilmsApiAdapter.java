package com.example.uah.mymovietracker.io;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Clase encargada de instanciar el objeto Retrofit para realizar las peticiones HTTP a la API de
 * OMDB
 * <p>
 * Fuentes:
 * https://programacionymas.com/blog/consumir-una-api-usando-retrofit
 * https://guides.codepath.com/android/consuming-apis-with-retrofit
 */
public class OmdbFilmsApiAdapter {
    private static OmdbFilmsApiService API_SERVICE;

    public static OmdbFilmsApiService getApiService() {

        // Creamos un interceptor y le indicamos el log level a usar
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        // Asociamos el interceptor a las peticiones
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        String baseUrl = "https://www.omdbapi.com";

        if (API_SERVICE == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build();
            API_SERVICE = retrofit.create(OmdbFilmsApiService.class);
        }

        return API_SERVICE;

    }
}
