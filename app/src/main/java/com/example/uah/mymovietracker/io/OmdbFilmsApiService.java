package com.example.uah.mymovietracker.io;

import com.example.uah.mymovietracker.io.response.OmdbFilm;
import com.example.uah.mymovietracker.io.response.OmdbSearch;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface OmdbFilmsApiService {

    // Método para realizar la búsqueda de una película a partir de su nombre
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @GET("/")
    public Call<OmdbSearch> getFilmsByName(@Query("apikey") String apikey, @Query("s") String title, @Query("type") String type);


    // Método para realizar la búsqueda de una película a partir del identificador de OMDB
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @GET("/")
    public Call<OmdbFilm> getFilmByOmdbId(@Query("apikey") String apikey, @Query("i") String omdbId);

}
