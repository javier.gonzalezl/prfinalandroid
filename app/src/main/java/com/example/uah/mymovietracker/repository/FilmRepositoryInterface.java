package com.example.uah.mymovietracker.repository;

import com.example.uah.mymovietracker.db.entity.FilmEntity;
import com.example.uah.mymovietracker.io.response.OmdbFilm;
import com.example.uah.mymovietracker.io.response.OmdbSearch;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public interface FilmRepositoryInterface {
    List<FilmEntity> getAllFilms();

    FilmEntity getFilmById(int id);

    void saveFilm(FilmEntity film);

    void updateFilm(FilmEntity film);

    void deleteFilm(FilmEntity film);

    Call<OmdbFilm> searchFilmByOmdbId(String omdbId);

    Call<OmdbSearch> searchFilmsByName(String title);
}