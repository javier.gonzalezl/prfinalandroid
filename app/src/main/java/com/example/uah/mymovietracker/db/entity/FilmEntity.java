package com.example.uah.mymovietracker.db.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.example.uah.mymovietracker.model.Film;

import java.util.Date;

/**
 * Una entidad representa una tabla de SQLite, sirviendo como modelo de datos para nuestra aplicación
 * <p>
 * Fuente: https://developer.android.com/codelabs/android-room-with-a-view#4
 */
@Entity(tableName = "films")
public class FilmEntity implements Film {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int id;

    @ColumnInfo(name = "omdb_id")
    private String omdbId;

    private String title;

    private String actors;

    private String year;

    private String poster;

    @ColumnInfo(name = "date_watched")
    private Date dateWatched;

    @ColumnInfo(name = "place_watched")
    private String placeWatched;

    @Ignore
    public FilmEntity(String omdbId, String title, String actors, String year, String poster, Date dateWatched, String placeWatched) {
        this.omdbId = omdbId;
        this.title = title;
        this.actors = actors;
        this.year = year;
        this.poster = poster;
        this.dateWatched = dateWatched;
        this.placeWatched = placeWatched;
    }

    public FilmEntity(int id, String omdbId, String title, String actors, String year, String poster, Date dateWatched, String placeWatched) {
        this.id = id;
        this.omdbId = omdbId;
        this.title = title;
        this.actors = actors;
        this.year = year;
        this.poster = poster;
        this.dateWatched = dateWatched;
        this.placeWatched = placeWatched;
    }


    @Override
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String getOmdbId() {
        return omdbId;
    }

    public void setOmdbId(String omdbId) {
        this.omdbId = omdbId;
    }

    @Override
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String getActors() {
        return actors;
    }

    public void setActors(String actors) {
        this.actors = actors;
    }

    @Override
    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    @Override
    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    @Override
    public Date getDateWatched() {
        return dateWatched;
    }

    public void setDateWatched(Date dateWatched) {
        this.dateWatched = dateWatched;
    }

    @Override
    public String getPlaceWatched() {
        return placeWatched;
    }

    public void setPlaceWatched(String placeWatched) {
        this.placeWatched = placeWatched;
    }

    @Override
    public String toString() {
        return "FilmEntity{" +
                "id=" + id +
                ", omdbId='" + omdbId + '\'' +
                ", title='" + title + '\'' +
                ", actors='" + actors + '\'' +
                ", year='" + year + '\'' +
                ", poster='" + poster + '\'' +
                ", dateWatched=" + dateWatched +
                ", placeWatched='" + placeWatched + '\'' +
                '}';
    }
}
