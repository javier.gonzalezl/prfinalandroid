package com.example.uah.mymovietracker.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.os.Bundle;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.uah.mymovietracker.MyMovieTrackerApp;
import com.example.uah.mymovietracker.R;
import com.example.uah.mymovietracker.db.entity.FilmEntity;

import com.example.uah.mymovietracker.repository.FilmRepository;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Actividad para visualizar los detalles de una película que ha visto el usuario.
 * También permite el borrado de la película
 */
public class FilmActivity extends AppCompatActivity {

    private FilmRepository mRepository;

    private FilmEntity mFilm;

    private TextView mTitle;
    private TextView mYear;
    private TextView mActors;
    private TextView mDate;
    private TextView mPlace;
    private ImageView mImage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film);

        // Accedemos al Singleton del repositorio de datos
        mRepository = ((MyMovieTrackerApp) getApplication()).getRepository();

        mTitle = findViewById(R.id.title_fa);
        mYear = findViewById(R.id.year_fa);
        mActors = findViewById(R.id.actors_fa);
        mImage = findViewById(R.id.image_fa);
        mDate = findViewById(R.id.date_fa);
        mPlace = findViewById(R.id.place_fa);

        Bundle bundle = this.getIntent().getExtras();
        int id = bundle.getInt(MyMovieTrackerApp.EXTRA_ID);
        loadFilm(id);
    }

    /**
     * Método para cargar la película desde la BBDD a partir del id recibido desde el Intent
     *
     * @param id
     */
    private void loadFilm(int id) {
        mFilm = mRepository.getFilmById(id);
        populateLayout();
    }

    /**
     * Método para inicializar los textos del layout en función del resultado obtenido en la API de OMDB
     */
    private void populateLayout() {
        mTitle.setText(mFilm.getTitle());
        mActors.setText(mFilm.getActors());
        mYear.setText(mFilm.getYear());
        Picasso.get().load(mFilm.getPoster()).error(R.drawable.ic_baseline_broken_image_24).into(mImage);

        DateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy");
        String dateFormatted = dateFormat.format(mFilm.getDateWatched());

        mDate.setText(dateFormatted);
        mPlace.setText(mFilm.getPlaceWatched());
    }

    /**
     * Muestra un cuadro de diálogo para que el usuario confirme si desea continuar con la operación de borrado
     */
    protected void showConfirmActionDialog() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.form_delete_question_dialog)
                .setPositiveButton(R.string.yes, (dialog, which) -> {
                    mRepository.deleteFilm(mFilm);
                    Toast.makeText(getApplicationContext(), R.string.film_deleted_msg, Toast.LENGTH_LONG).show();
                    finish();
                })
                .setNegativeButton(R.string.back, (dialog, which) -> dialog.cancel())
                .show();
    }

    /**
     * Manejador del evento Click del botón usado para eliminar una película
     *
     * @param view
     */
    public void delete(View view) {
        showConfirmActionDialog();
    }
}