package com.example.uah.mymovietracker.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.uah.mymovietracker.MyMovieTrackerApp;
import com.example.uah.mymovietracker.R;
import com.example.uah.mymovietracker.db.entity.FilmEntity;
import com.example.uah.mymovietracker.io.response.OmdbFilm;
import com.example.uah.mymovietracker.repository.FilmRepository;
import com.example.uah.mymovietracker.ui.dialog.DatePickerFragment;
import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Actividad para visualizar los detalles de uno de los resultados de la búsqueda por título.
 * También permite al usuario añadir la película a su colección de peliculas vistas.
 */
public class OmdbFilmActivity extends AppCompatActivity implements Callback<OmdbFilm>, View.OnClickListener {

    private FilmRepository mRepository;

    private OmdbFilm mFilm;

    private TextView mTitle;
    private TextView mYear;
    private TextView mActors;
    private EditText mDate;
    private EditText mPlace;
    private ImageView mImage;

    private Date mDateWatched;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_omdb_film);

        // Accedemos al Singleton del repositorio de datos
        mRepository = ((MyMovieTrackerApp) getApplication()).getRepository();

        mTitle = findViewById(R.id.title_ofa);
        mYear = findViewById(R.id.year_ofa);
        mActors = findViewById(R.id.actors_ofa);
        mImage = findViewById(R.id.image_ofa);
        mDate = findViewById(R.id.input_date_ofa);
        mPlace = findViewById(R.id.input_place_ofa);

        // Asignación de click listener al input de la fecha para que despliegue su cuadro de diálogo
        mDate.setOnClickListener(this);

        Bundle bundle = this.getIntent().getExtras();
        String omdb_id = bundle.getString(MyMovieTrackerApp.EXTRA_OMDB_ID);
        loadOmdbFilm(omdb_id);
    }


    @Override
    /**
     * Callback que se ejecuta cuando la petición de Retrofit tiene éxito
     */
    public void onResponse(Call<OmdbFilm> call, Response<OmdbFilm> response) {
        if(response.isSuccessful()){
            mFilm = response.body();
            populateLayout();
        }
    }

    @Override
    /**
     * Callback que se ejecuta ante un error en la petición de Retrofit
     */
    public void onFailure(Call<OmdbFilm> call, Throwable t) {
        Toast toast = Toast.makeText(this,
                "Ha ocurrido un error", Toast.LENGTH_LONG);
        toast.show();
    }

    /**
     * Inicia la búsqueda de una película a partir de su OMDB_ID
     * @param id
     */
    private void loadOmdbFilm(String id) {
        Call<OmdbFilm> call = mRepository.searchFilmByOmdbId(id);
        call.enqueue(this);
    }

    /**
     * Método para inicializar los textos del layout en función del resultado obtenido en la API de OMDB
     */
    private void populateLayout() {
        mTitle.setText(mFilm.getTitle());
        mActors.setText(mFilm.getActors());
        mYear.setText(mFilm.getYear());
        Picasso.get().load(mFilm.getImage()).error(R.drawable.ic_baseline_broken_image_24).into(mImage);
    }

    @Override
    /**
     * Manejador del evento onClick usado para capturar la pulsación del input de la fecha de visualización
     */
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.input_date_ofa:
                showDatePickerDialog();
                break;
        }
    }

    /**
     * Muestra un cuadro de diálogo para que el usuario confirme si desea continuar con la operación de inserción
     */
    protected void showConfirmActionDialog() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.form_add_question_dialog)
                .setPositiveButton(R.string.yes, (dialog, which) -> {
                    mRepository.saveFilm(new FilmEntity(mFilm.getImdbID(), mFilm.getTitle(),
                            mFilm.getActors(), mFilm.getYear(), mFilm.getImage(), mDateWatched, mPlace.getText().toString()));
                    Toast.makeText(getApplicationContext(), R.string.film_added_msg, Toast.LENGTH_LONG).show();
                    finish();
                })
                .setNegativeButton(R.string.back, (dialog, which) -> dialog.cancel())
                .show();
    }

    /**
     * Abre el cuadro de diálogo que permite elegir una fecha a partir de un calendario interactivo
     */
    private void showDatePickerDialog() {
        DatePickerFragment newFragment = DatePickerFragment.newInstance((view, year, month, dayOfMonth) -> {
            final String selectedDate = twoDigits(dayOfMonth) + "/" + twoDigits(month+1) + "/" + year;
            mDate.setText(selectedDate);
            Calendar cal = Calendar.getInstance();
            cal.set(year, month, dayOfMonth);
            mDateWatched =cal.getTime();
        });
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    private String twoDigits(int n) {
        return (n<=9) ? ("0"+n) : String.valueOf(n);
    }

    /**
     * Manejador del evento Click del botón usado para añadir una película
     *
     * @param view
     */
    public void addFilm(View view) {
        showConfirmActionDialog();
    }
}