package com.example.uah.mymovietracker.db.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import com.example.uah.mymovietracker.db.entity.FilmEntity;

import java.util.List;

/**
 * La anotación @Dao identifica esta clase como un DAO para Room.
 * <p>
 * Un Dao es componente de arquitectura
 * que valida el SQL en tiempo de compilación y lo asocia a uno de nuestros métodos. Con las anotaciones
 * de Room podemos implementar las operaciones más simples como insert, update o delete, al mismo tiempo
 * que si queremos ejecutar una consulta algo más compleja podemos usar la anotación @Query indicando
 * dicha consulta.
 * <p>
 * Fuente: https://developer.android.com/codelabs/android-room-with-a-view#5
 */
@Dao
public interface FilmDao {

    @Query("SELECT * FROM films WHERE id = :id")
    FilmEntity getFilmById(int id);

    @Query("SELECT * FROM films")
    List<FilmEntity> getAllFilms();

    @Insert()
    void insertFilm(FilmEntity film);

    @Update
    void updateFilm(FilmEntity film);

    @Delete
    void deleteFilm(FilmEntity film);
}
