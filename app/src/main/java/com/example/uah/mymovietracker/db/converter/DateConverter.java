package com.example.uah.mymovietracker.db.converter;

import androidx.room.TypeConverter;

import java.util.Date;

/**
 * Clase para poder almacenar y recuperar de Room los objetos de tipo Date
 */
public class DateConverter {
    @TypeConverter
    public static Date toDate(Long timestamp) {
        return timestamp == null ? null : new Date(timestamp);
    }

    @TypeConverter
    public static Long toTimestamp(Date date) {
        return date == null ? null : date.getTime();
    }

}
