package com.example.uah.mymovietracker.io.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Clase para convertir la respuesta recibida de una petición mediante Retrofit a la API de búsquedas
 * por id de OMDB
 */
public class OmdbFilm {
    @SerializedName("Title")
    @Expose
    private String title;

    @SerializedName("Year")
    @Expose
    private String year;

    @SerializedName("Actors")
    @Expose
    private String actors;

    @SerializedName("Poster")
    @Expose
    private String image;

    @SerializedName("imdbID")
    @Expose
    private String imdbID;

    public OmdbFilm(String title, String year, String actors, String image, String imdbID) {
        this.title = title;
        this.year = year;
        this.actors = actors;
        this.image = image;
        this.imdbID = imdbID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getActors() {
        return actors;
    }

    public void setActors(String actors) {
        this.actors = actors;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImdbID() {
        return imdbID;
    }

    public void setImdbID(String imdbID) {
        this.imdbID = imdbID;
    }
}
