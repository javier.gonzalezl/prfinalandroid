package com.example.uah.mymovietracker.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.uah.mymovietracker.MyMovieTrackerApp;
import com.example.uah.mymovietracker.R;
import com.example.uah.mymovietracker.io.response.OmdbFilm;
import com.example.uah.mymovietracker.io.response.OmdbSearch;
import com.example.uah.mymovietracker.repository.FilmRepository;
import com.example.uah.mymovietracker.ui.adapter.OmdbFilmListAdapter;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Actividad que permite la búsqueda de películas y la visualización del resultado de dicha búsqueda
 */
public class SearchActivity extends AppCompatActivity implements Callback<OmdbSearch> {

    private FilmRepository mRepository;

    private RecyclerView mRecyclerView;
    private OmdbFilmListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        // Accedemos al Singleton del repositorio de datos
        mRepository = ((MyMovieTrackerApp) getApplication()).getRepository();

        mRecyclerView = findViewById(R.id.reyclerview_search);
        // Creamos el adaptador y le pasamos como argumentos los datos para generar cada elemento de la lista
        mAdapter = new OmdbFilmListAdapter(this,  new ArrayList());
        // Conectamos el adaptador con el RecyclerView
        mRecyclerView.setAdapter(mAdapter);
        // Definimos el tipo de layout manager que tendrá el RecyclerView por defecto
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    /**
     * Inicia la búsqueda de las peliculas a partir del título introducido por el usuario
     * @param view
     */
    public void search(View view) {
        EditText movie = (EditText) findViewById(R.id.editText_film);
        Call<OmdbSearch> call = mRepository.searchFilmsByName(movie.getText().toString());
        call.enqueue(this);
    }

    @Override
    /**
     * Callback que se ejecuta cuando la petición de Retrofit tiene éxito
     */
    public void onResponse(Call<OmdbSearch> call, Response<OmdbSearch> response) {
        if (response.isSuccessful()) {
            OmdbSearch result = response.body();
            ArrayList<OmdbFilm> films = result.getResults();
            mAdapter.setFilmList(films);
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    /**
     * Callback que se ejecuta ante un error en la petición de Retrofit
     */
    public void onFailure(Call<OmdbSearch> call, Throwable t) {
        Toast toast = Toast.makeText(this,
                "Ha ocurrido un error", Toast.LENGTH_LONG);
        toast.show();
    }
}