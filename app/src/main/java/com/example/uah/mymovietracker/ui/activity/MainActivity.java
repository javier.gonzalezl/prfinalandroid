package com.example.uah.mymovietracker.ui.activity;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


import com.example.uah.mymovietracker.MyMovieTrackerApp;
import com.example.uah.mymovietracker.R;
import com.example.uah.mymovietracker.db.entity.FilmEntity;
import com.example.uah.mymovietracker.repository.FilmRepository;
import com.example.uah.mymovietracker.ui.adapter.FilmListAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

/**
 * Actividad principal de la aplicación.
 * Muestra el listado de películas que el usuario ha marcado como visualizadas-
 */
public class MainActivity extends AppCompatActivity {

    private FilmRepository mRepository;

    private RecyclerView mRecyclerView;
    private FilmListAdapter mAdapter;

    private ArrayList<FilmEntity> mFilmList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Accedemos al Singleton del repositorio de datos
        mRepository = ((MyMovieTrackerApp) getApplication()).getRepository();
        loadFilms();

        mRecyclerView = findViewById(R.id.reyclerview_main);
        // Creamos el adaptador y le pasamos como argumentos los datos para generar cada elemento de la lista
        mAdapter = new FilmListAdapter(this, mFilmList);
        // Conectamos el adaptador con el RecyclerView
        mRecyclerView.setAdapter(mAdapter);
        // Definimos el tipo de layout manager que tendrá el RecyclerView por defecto
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    /**
     * Capturamos el estado onResume del ciclo de vida de la apliación para volver a cargar el
     * listado de películas, ya que es posible que se haya actualizado
     */
    public void onResume(){
        super.onResume();
        loadFilms();
        mAdapter.setFilmList(mFilmList);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    /**
     * Manejador para detectar la pulsación del botón para salir de la aplicación y mostrar
     * un cuadro de diálogo alertando al usuario de las consecuencias de su acción
     */
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            new AlertDialog.Builder(this)
                    .setMessage(R.string.exit_dialog)
                    .setPositiveButton(R.string.yes, (dialog, which) -> finish())
                    .setNegativeButton(R.string.back, (dialog, which) -> dialog.cancel())
                    .show();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * Obtiene las peliculas que hay en la base de datos
     */
    private void loadFilms(){
        mFilmList = mRepository.getAllFilms();
    }


}